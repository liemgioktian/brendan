<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="xsanisty">
    <title>Will Instruction Sheet</title>
    <link rel="stylesheet" href="asset/css/bootstrap.css">
    <link href="asset/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="asset/css/style.css" />
</head>
<body>
<div class="container">
    <form class="form-horizontal" action="pdf2.php" method="post" role="form" id="brendan2" target="_blank">
        <input type="hidden" name="action" id="action">
        <div class="row">
            <div class="col-md-12 text-center page-header">
                    <img class="img" src='asset/img/rachel_logo.png' style="width:125px" alt="Will Instruction Sheet">
                    <h1 class="bold red">Will Instruction Sheet</h1>
            </div>
        </div>
        <div class="row">
            <div class="">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 5.55%;">
                            page 1
                    </div>
                </div>
            </div>
        </div>
        
    	<div id="page1" class="page">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="client" class="col-sm-4 control-label pull_left">Client</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-md" id="client" name="client">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_issued" class="col-sm-4 control-label pull_left">Date sheet issued</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-md datepicker" id="date_issued" name="date_issued">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>  
    	    
            <div class="row">
                <div class="col-md-12">
                    <h4 class="bold" >Introduction</h4>
                    <hr style="margin-top:0px;">
                </div>
            </div>
            

            <div class="row">
                <div class="col-md-12">
                    <p class="justify">
                        The information requested in this Instruction Sheet is necessary to enable us to properly review and advise you in relation to your new or updated Will and/or Power of Attorney and/or Enduring Power of Guardianship. It’s really important to be honest and include all of the details requested if they apply to you. If you don’t provide all of the information requested by us this may result in us drafting documents for you which do not appropriately address your individual needs and which may result in unwanted results during your lifetime or after you pass away.
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p class="justify">
                        We confirm all information supplied will remain private and confidential at all times.  We will not disclose any client information to a third party unless instructed to do so or where required by law.
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p class="justify">
                        There are three (3) parts to this Instruction Sheet:
                    </p>
                    <ul>
                        <li>
                            <p class="justify"><u>Part 1: </u> Asks questions about your personal situation, who you are, who’s in your family and your assets and liabilities;</p>
                        </li>
                        <li>
                            <p class="justify"><u>Part 2: </u> Asks for your instructions for your new Will, who you want to be your executor, beneficiaries etc; and</p>
                        </li>
                        <li>
                            <p class="justify"><u>Part 3: </u> Asks for your instructions for your new Power of Attorney and/or Enduring Power of Guardianship.</p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p class="justify">
                        We ask that you please full complete and return the Instruction Sheet to us as soon as possible so that we can commence drafting your Will. 
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Initial Details</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                     <tr>
                                         <td>&nbsp;</td>
                                         <td class="text-center"><label for="">Client</label></td>
                                     </tr>
                                     <tr>
                                         <td><label for="">1. Do you have a Will?</label></td>
                                         <td class="text-center">
                                            <label class="checkbox-inline bold">
                                                <input type="checkbox" value="1" id="client11" name="client1">Y
                                            </label>
                                            <label class="checkbox-inline bold">
                                                <input type="checkbox" value="0" id="client12" name="client1">N ( if no go to Q 4)
                                            </label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td><label for="">2. When was it last reviewed?</label></td>
                                         <td>
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <input type="text" class="form-control input-sm" id="reviewed" name="reviewed">
                                                </div>
                                            </div>
                                        </td>
                                     </tr>
                                     <tr>
                                         <td><label for="">3. Where is it located?</label></td>
                                         <td>
                                             <div class="">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4">solicitors name</label>
                                                        <div class="col-sm-8">
                                                             <input type="text" class="form-control input-sm" id="solicitor" name="solicitor">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4">firm name</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control input-sm" id="firm" name="firm">
                                                        </div>
                                                    </div>
                                                 </div>
                                                 <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4">address</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control input-sm" id="address1" name="address1">
                                                        </div>
                                                    </div>
                                                 </div>
                                             </div>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td></td>
                                         <td>
                                            <label for="" class="col-sm-6 control-label pull_left">Do you authorize us the retrieve it </label>
                                            <div class="col-sm-6"> 
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="client21" name="client2">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="client22" name="client2">N
                                                </label>
                                            </div>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td><label for="">4. Do you have an Enduring Power of Attorney?</label></td>
                                         <td class="text-center">
                                            <label class="checkbox-inline bold">
                                                <input type="checkbox" value="1" id="client31" name="client3">Y
                                            </label>
                                            <label class="checkbox-inline bold">
                                                <input type="checkbox" value="0" id="client32" name="client3">N
                                            </label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td><label for="">5. Do you have an Enduring Power of Guardianship?</label></td>
                                         <td class="text-center">
                                            <label class="checkbox-inline bold">
                                                <input type="checkbox" value="1" id="client41" name="client4">Y
                                            </label>
                                            <label class="checkbox-inline bold">
                                                <input type="checkbox" value="0" id="client42" name="client4">N
                                            </label>
                                         </td>
                                     </tr>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p class="justify">
                        If you answered yes to any of the above questions please provide us with a copy of the document /s, if available. 
                    </p>
                </div>
            </div>

            <div class="row space">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-lg btn-primary next" id="next2" name="next2" data-next="2">NEXT</button>
                    <button type="button" class="btn btn-lg btn-primary next" id="end" name="end" data-next="18">END</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class="col-md-12">
                    <p class="text-right bold">
                        (Page 1 of 18)
                    </p>
                </div>
            </div>

    	</div><!--close page1-->

        <div id="page2" class="page hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="bold"><u>PART 1</u></h3>
                        </div>
                    </div>
                    <div class="row space">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Personal Details</u></h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Basic Information</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                             <th style=""><label for="" class="">&nbsp;</label></th>
                                             <th class="text-center"><label for="" class="">Client</label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <tr>
                                             <td><label for="">Title</label></td>
                                             <td class="">
                                                <select name="title" id="title">
                                                    <option value="Mr.">Mr.</option>
                                                    <option value="Ms.">Ms.</option>
                                                    <option value="Mrs.">Mrs.</option>
                                                    <option value="Dr.">Dr.</option>
                                                </select>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Surname</label></td>
                                             <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="surname" name="surname">
                                                    </div>
                                                </div>
                                            </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Given Names</label></td>
                                             <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="given_names" name="given_names">
                                                    </div>
                                                </div>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Date of Birth</label></td>
                                             <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm datepicker" id="dob" name="dob">
                                                    </div>
                                                </div>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Sex</label></td>
                                             <td class="">
                                                <select name="sex" id="sex">
                                                    <option value="M">M</option>
                                                    <option value="F">F</option>
                                                </select>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Marital Status</label></td>
                                             <td class="">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="marital1" name="marital">Married
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="2" id="marital2" name="marital">Single
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="3" id="marital3" name="marital">Divorced 
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="4" id="marital4" name="marital">Widowed 
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="5" id="marital5" name="marital">Defacto 
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="6" id="marital6" name="marital">Separated
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="7" id="marital7" name="marital">Same Sex Domestic Relationship 
                                                </label>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Current Partners Name</label></td>
                                             <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="partner_name" name="partner_name">
                                                    </div>
                                                </div>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Relationship commenced</label></td>
                                             <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="commenced" name="commenced">
                                                    </div>
                                                </div>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Residential Address</label></td>
                                             <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="res_address" name="res_address">
                                                    </div>
                                                </div>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td><label for="">Postal Address if different</label></td>
                                             <td>
                                                 <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="postal_address" name="postal_address">
                                                    </div>
                                                </div>
                                             </td>
                                         </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Basic Information</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <tr>
                                        <td><label for="">Contact Numbers</label></td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-1">H :</label>
                                                    <div class="col-sm-11">
                                                         <input type="text" class="form-control input-sm" id="hoem_phone" name="hoem_phone">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-1">M :</label>
                                                    <div class="col-sm-11">
                                                        <input type="text" class="form-control input-sm" id="mobile_phone" name="mobile_phone">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-1">W :</label>
                                                    <div class="col-sm-11">
                                                        <input type="text" class="form-control input-sm" id="work_phone" name="work_phone">
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="">Fax</label></td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="fax" name="fax">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="">Email</label></td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="email" name="email">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Children</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for="">Name</label></th>
                                            <th class="text-center"><label for="">Mutual Child</label></th>
                                            <th class="text-center"><label for="">Age</label></th>
                                            <th class="text-center"><label for="">Financially Dependent</label></th>
                                            <th class="text-center"><label for="">Address</label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_child1" name="name_child1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="mutual11" name="mutual1">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="mutual12" name="mutual1">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="age_child1" name="age_child1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="financial_depend11" name="financial_depend1">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="financial_depend12" name="financial_depend1">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_child1" name="address_child1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_child2" name="name_child2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="mutual21" name="mutual2">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="mutual22" name="mutual2">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="age_child2" name="age_child2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="financial_depend21" name="financial_depend2">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="financial_depend22" name="financial_depend2">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_child2" name="address_child2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_child3" name="name_child3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="mutual31" name="mutual3">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="mutual32" name="mutual3">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="age_child3" name="age_child3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="financial_depend31" name="financial_depend3">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="financial_depend32" name="financial_depend3">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_child3" name="address_child3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_child4" name="name_child4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="mutual41" name="mutual4">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="mutual42" name="mutual4">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="age_child4" name="age_child4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="financial_depend41" name="financial_depend4">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="financial_depend42" name="financial_depend4">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_child4" name="address_child4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_child5" name="name_child5">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="mutual51" name="mutual5">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="mutual52" name="mutual5">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="age_child5" name="age_child5">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="financial_depend51" name="financial_depend5">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="financial_depend52" name="financial_depend5">N
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_child5" name="address_child5">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>
            
            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back1" name="back1" data-back="1">BACK</button>
                </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next3" name="next3" data-next="3">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class="col-md-12">
                    <p class="text-right bold">
                        (Page 2 of 18)
                    </p>
                </div>
            </div>
            
        </div><!--close page2-->

        <div id="page3" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="bold"><u>Other Dependants</u></h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" id="add_dependants" class="btn btn-sm btn-success">add more</button>
                            <input type="hidden" name="count_dependants" value="4" />
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for="">Name</label></th>
                                            <th class="text-center"><label for="">Relationship to you</label></th>
                                            <th class="text-center"><label for="">Age</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="dependants_row">
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_depend1" name="name_depend1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="relation_depend1" name="relation_depend1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_depend1" name="address_depend1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_depend2" name="name_depend2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="relation_depend2" name="relation_depend2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_depend2" name="address_depend2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_depend3" name="name_depend3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="relation_depend3" name="relation_depend3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_depend3" name="address_depend3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="name_depend4" name="name_depend4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="relation_depend4" name="relation_depend4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="address_depend4" name="address_depend4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Former Relationships</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Client</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td>
                                                <p class="justify">Have you been previously married or in a de facto <br> relationship (2 years or more cohabiting or sharing finances to some extent)? </p>
                                                <p class="justify">If yes, please complete:</p>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="former11" name="former1">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="former12" name="former1">N
                                                </label>
                                                <p>(if no go to employment details)</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Former partner’s name</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="former_name" name="former_name">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Relationship duration</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="relation_dur" name="relation_dur">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">When did the relationship end?</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="relation_end" name="relation_end">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Were any orders or agreements made about property or children of the relationship? <br> If yes, please provide details:</label></td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="former21" name="former2">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="former22" name="former2">N
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="" class="control-label col-sm-4 pull_left">
                                                    Where there any relationships prior this 
                                                </label>
                                            </td>
                                            <td class="text-center">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="former31" name="former31" class="former3">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="former32" name="former32" class="former3">N
                                                </label>
                                                <textarea type="text" class="hidden form-control input-sm" id="relation_prior" name="relation_prior" cols="3" rows="3"></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Employment Details</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Client</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td><label for="">Employment</label></td>
                                            <td>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="employment1" name="employment">Employed
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="2" id="employment2" name="employment">Self-employed 
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="3" id="employment3" name="employment">Unemployed
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Occupation</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="occupation" name="occupation">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back2" name="back2" data-back="2">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next4" name="next4" data-next="4">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 3 of 18)
                    </p>
                </div>
            </div>

        </div><!-- close page3 -->

        <div id="page4" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="bold"><u>Financial Details</u></h4>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Income</u></h4>
                        </div>
                        
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Client</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td><label for="">Gross Annual Income</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="gross_income" name="gross_income">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Are you receiving Social Security benefits?</label></td>
                                            <td>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="income1" name="income">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="income2" name="income">N
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Any other income?:</label></td>
                                            <td>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="onther_income1" name="onther_income">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="onther_income2" name="onther_income">N
                                                </label>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="bold"><u>Assets</u></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="bold">Principal Residence</label>
                        </div>
                    </div>
                    
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <tbody id="">
                                        <tr>
                                            <td><label for="">Owner/s</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="owners1" name="owners1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <select name="owners2" id="owners2" class="input-sm form-control">
                                                            <option value="Tenants in Common">Tenants in Common </option>
                                                            <option value="Joint Tenants">Joint Tenants </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Value</label></td>
                                            <td colspan="2">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="owners_value" name="owners_value">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Mortgage to:</label></td>
                                            <td colspan="2">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="owners_mortgage" name="owners_mortgage">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Amount owing on mortgage:</label></td>
                                            <td colspan="2">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="owners_owing" name="owners_owing">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">Date Acquired</label></td>
                                            <td colspan="2">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm datepicker" id="owners_acquired" name="owners_acquired">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Other Assets</u></h4>
                        </div>
                        
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Owner</label></th>
                                            <th class="text-center"><label for="">Asset Value</label></th>
                                            <th class="text-center"><label for="">Details</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td><label for="">Household Contents</label></td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="household1" name="household1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="household2" name="household2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="household3" name="household3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Non-Income Producing Real Estate
                                                    (eg. holiday home, land)
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="non_income1" name="non_income1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="non_income2" name="non_income2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="non_income3" name="non_income3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Motor Vehicles
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="motor_vehicles1" name="motor_vehicles1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="motor_vehicles2" name="motor_vehicles2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="motor_vehicles3" name="motor_vehicles3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Boat / Marine
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="boat1" name="boat1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="boat2" name="boat2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="boat3" name="boat3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Caravan
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="caravan1" name="caravan1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="caravan2" name="caravan2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="caravan3" name="caravan3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Jewellery
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="jewellery1" name="jewellery1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="jewellery2" name="jewellery2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="jewellery3" name="jewellery3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Collectibles / Art / Other
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="art1" name="art1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="art2" name="art2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="art3" name="art3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Business
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="business1" name="business1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="business2" name="business2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="business3" name="business3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="">
                                                    Others
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="others1" name="others1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="others2" name="others2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="others3" name="others3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back3" name="back3" data-back="3">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next5" name="next5" data-next="5">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 4 of 18)
                    </p>
                </div>
            </div>

        </div><!-- close page4 -->

        <div id="page5" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Investment Assets</u></h4>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="bold">Current Investments / Bank Accounts/ Savings     (excluding investment properties)</h4>
                        </div>
                        <div class="col-md-4 text-right">
                            <button type="button" id="add_invest_assets" class="btn btn-sm btn-success">add more</button>
                            <input type="hidden" name="count_invest_assets" value="4" />
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Description / Account number </label></th>
                                            <th class="text-center"><label for="">Owner</label></th>
                                            <th class="text-center"><label for="">Current Value</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="invest_assets_row">
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="invest_no1" name="invest_no1" value="1">
                                                <label for="" id="investment_no1" name="investment_no1" data-formula="$invest_no1"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_account1" name="invest_account1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_owner1" name="invest_owner1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_value1" name="invest_value1" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="invest_no2" name="invest_no2" value="2">
                                                <label for="" id="investment_no2" name="investment_no2" data-formula="$invest_no2"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_account2" name="invest_account2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_owner2" name="invest_owner2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_value2" name="invest_value2" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="invest_no3" name="invest_no3" value="3">
                                                <label for="" id="investment_no3" name="investment_no3" data-formula="$invest_no3"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_account3" name="invest_account3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_owner3" name="invest_owner3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_value3" name="invest_value3" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="invest_no4" name="invest_no4" value="4">
                                                <label for="" id="investment_no4" name"investment_no4" data-formula="$invest_no4"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_account4" name="invest_account4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_owner4" name="invest_owner4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="invest_value4" name="invest_value4" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="bold"><u>Investment Property</u></h4>
                        </div>
                        <div class="col-md-4 text-right">
                            <button type="button" id="add_invest_property" class="btn btn-sm btn-success">add more</button>
                            <input type="hidden" name="count_invest_property" value="4" />
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Address</label></th>
                                            <th class="text-center"><label for="">Owner</label></th>
                                            <th class="text-center"><label for="">Date Purchased</label></th>
                                            <th class="text-center"><label for="">Value</label></th>
                                            <th class="text-center"><label for="">Current Loan Balance</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="invest_property_row">
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="property_no1" name="property_no1" value="1">
                                                <label for="" id="invest_property_no1" name="invest_property_no1" data-formula="$property_no1"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_address1" name="property_address1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_owner1" name="property_owner1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm datepi" id="property_date1" name="property_date1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_value1" name="property_value1" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_balance1" name="property_balance1" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="property_no2" name="property_no2" value="2">
                                                <label for="" id="invest_property_no2" name="invest_property_no2" data-formula="$property_no2"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_address2" name="property_address2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_owner2" name="property_owner2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm datepi" id="property_date2" name="property_date2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_value2" name="property_value2" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_balance2" name="property_balance2" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="property_no3" name="property_no3" value="3">
                                                <label for="" id="invest_property_no3" name"invest_property_no3" data-formula="$property_no3"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_address3" name="property_address3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_owner3" name="property_owner3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm datepi" id="property_date3" name="property_date3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_value3" name="property_value3" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_balance3" name="property_balance3" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="text" class="hidden" id="property_no4" name="property_no4" value="4">
                                                <label for="" id="invest_property_no4" name"invest_property_no4" data-formula="$property_no4"></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_address4" name="property_address4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_owner4" name="property_owner4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm datepi" id="property_date4" name="property_date4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_value4" name="property_value4" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="property_balance4" name="property_balance4" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="bold"><u>Superannuation</u></h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><div class="col-sm-6"><label for=""></label></th></div>
                                            <th class="text-center"><div class="col-sm-6"><label for=""></label></th></div>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for="">Fund Name</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="fund_name1" name="fund_name1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Current Account Balance</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="super_balance1" name="super_balance1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                    Death Benefit Nomination Details? <br>
                                                    (Have you nominated the person who is to receive your super <br> balance and/or life insurance when you die?)
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="super_death1" name="super_death1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                    If you have a Death Benefit Nomination, <br> is it binding or non binding?
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="super_binding1" name="super_binding1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back4" name="back4" data-back="4">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next6" name="next6" data-next="6">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class="col-md-15">
                    <p class="text-right bold">
                        (Page 5 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page5 -->

        <div id="page6" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="bold"><u>Liabilities</u></h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="button" class="btn btn-sm btn-success" id="add_liabilities">add more</button>
                            <input type="hidden" name="count_liabilities" value="3" />                            
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Current Loan Balance</label></th>
                                            <th class="text-center"><label for="">Bank / person responsible </label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="liabilities_row">
                                        <tr>
                                            <td class="">
                                                <label for="">Principal Residence</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="liabilitas_loan1" name="liabilitas_loan1" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="liabilitas_bank1" name="liabilitas_bank1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Investment Loan 1</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="liabilitas_loan2" name="liabilitas_loan2" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="liabilitas_bank2" name="liabilitas_bank2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Investment Loan 2</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="liabilitas_loan2" name="liabilitas_loan3" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="liabilitas_bank2" name="liabilitas_bank3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="bold"><u>Liabilities other</u></h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="button" class="btn btn-sm btn-success" id="add_liabilities">add more</button>
                            <input type="hidden" name="count_liabilities_other" value="4" />
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Current Loan Balance</label></th>
                                            <th class="text-center"><label for="">Bank / person responsible </label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="liabilities_row">
                                        <tr>
                                            <td class="">
                                                <label for="">Car Loan</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_loan1" name="lia_other_loan1" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_bank1" name="lia_other_bank1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Personal Loan</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_loan2" name="lia_other_loan2" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_bank2" name="lia_other_bank2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Credit Card</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_loan3" name="lia_other_loan3" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_bank3" name="lia_other_bank3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Other</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_loan4" name="lia_other_loan4" data-format="$ 0,0[.]">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lia_other_bank4" name="lia_other_bank4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back5" name="back5" data-back="5">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next7" name="next7" data-next="7">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 6 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page6 -->

        <div id="page7" class="page hidden">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <hr>
                    <h4 class="bold">Other Assets</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">If you have any of the following insurances, assets, entities or structures in place, please as much detail as possible in relation to these below:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li><p class="justify"><u>Insurances: </u>Life and TPD Cover, Income Protection Insurance, Critical Illness Cover, General Home, Contents or Vehicle insurance</p></li>
                        <li><p class="justify"><u>Other Entities: </u>Self-Managed Superannuation Fund, Company, Partnership, Trust</p></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea type="text" class="form-control input-sm" id="page7_1" name="page7_1" cols="3" rows="12"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify space">Is there anything else which you would like us to know?  Please let us have your comments.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea type="text" class="form-control input-sm" id="page7_2" name="page7_2" cols="3" rows="12"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify space">Acknowledgement: I have read and understood all questions the above</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify space">Don’t understand anything  - No problem ask us.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-sm btn-warning" id="ask_part1">ask question</button>
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back6" name="back6" data-back="6">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next8" name="next8" data-next="8">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 7 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page7 -->

        <div id="page8" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3><u>PART 2</u></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>1. Instructions for your new Will</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If your affairs are straightforward, we have designed this form in order to capture all appropriate information we need to make a Will for you.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Each person wanting a Will should complete one of these forms.  For example, if both you and your spouse require Wills, please complete two forms.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">This form is not designed for people with complex affairs or people requiring a complex Will.  If this is your situation, you should contact us for an initial consultation.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If you otherwise have any questions about completing the form please telephone us on (02) 4229 8805 for assistance.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>2. Will any marriage, divorce or separation affect your Will?</h4>
                            <hr>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If you are in a defacto relationship or contemplating marriage, your Will should reflect these situations otherwise, a pre-existing Will is automatically revoked when you marry.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If you have divorced your existing Will may thereby revoked.  If you have recently separated and you have a Will, chances are you have left everything to your former spouse.  If you do not have a Will, the law assumes that he / she is your major beneficiary until you are divorced.  In these circumstances you should consider making a new Will.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>3. Property that is not affected by a Will</h4>
                            <hr>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">In some instances, property that you regard as your own may not, upon your death, form part of your estate.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Upon your death if you own property as a joint tenant with any person, that property is not affected by your Will as it will pass to the surviving joint owner(s).  Some examples are:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">Any real estate, for instance, the family home that you may own as a joint tenant with your spouse or another person;</p>
                                </li>
                                <li>
                                    <p class="justfify">Any jointly owned bank, building society or credit union accounts; and</p>
                                </li>
                                <li>
                                    <p class="justify">Any jointly owned shares.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">In the event of your death, entitlements under a superannuation or life insurance policy where benefits are payable to a nominated beneficiary are also not affected by your Will.</p>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back7" name="back7" data-back="7">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next9" name="next9" data-next="9">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 8 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page8 -->

        <div id="page9" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>4. The executor of your Will</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Your appointed executor is the person or persons nominated in your Will to administer your estate and carry out any of your wishes upon your death.  Careful consideration should be given to choosing an executor.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">The duties of an executor include the following:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">making all necessary arrangements for your funeral;</p>
                                </li>
                                <li>
                                    <p class="justify">paying any funeral or associated expenses and any debts of your estate;</p>
                                </li>
                                <li>
                                    <p class="justify">Notifying all beneficiaries named in your Will and any interested parties</p>
                                </li>
                                <li>
                                    <p class="justify">taking charge of and protecting your assets;</p>
                                </li>
                                <li>
                                    <p class="justify">selling and/or distributing your assets according to the directions in your Will.</p>
                                </li>
                                <li>
                                    <p class="justify">finalising your income tax returns; and</p>
                                </li>
                                <li>
                                    <p class="justify">obtaining an authority to administer your estate in the form of a Grant of Probate from the Supreme Court if necessary;</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Any reasonable out of pocket expenses and costs incurred by your executor are usually paid out of your estate.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Generally your executor is also entitled to retain professional assistance if he or she considers it necessary.</p>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>5. Who should you choose as an Executor.</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">You should choose someone reliable that you trust.  Any appointed executor is not bound to act for you and it is therefore a good idea to talk to them before appointing them.  Your executor does not need any special qualifications but must be willing to undertake the duty appointed on them.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If your executor considers that he/she requires professional assistance they are at liberty to engage a lawyer or other suitably qualified professional following your death.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If you wish, you may appoint more than one executor.  If you wish you can appoint one primary executor and then an alternate executor who is willing to undertake the duty if the primary executor is unable or unwilling to after your death.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">You may appoint one of your beneficiaries named in your Will as your executor or executors.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">The choice of executor is your decision.  The following may assist you in making that decision:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">If it is your intention to leave everything in your Will to one single person, it would be prudent to appoint that person as your executor.</p>
                                </li>
                                <li>
                                    <p class="justify">If it is your intention to leave everything in your Will to a spouse, if they survive you, it would be prudent to appoint that spouse as your primary executor.</p>
                                </li>
                                <li>
                                    <p class="justify">If it is your intention to leave everything in your Will to your children, in the event that you have no spouse, or, if they have predeceased you, then you may wish to consider appointing one or more of your children who are aged over 18 years of age as your executor or alternative executor.</p>
                                </li>
                                <li>
                                    <p class="justify">If you wish, you may also appoint a trusted family friend or relative.</p>
                                </li>
                                <li>
                                    <p class="justify">You may also wish to make a direction in your Will that your executor engage a solicitor to ensure that they obtain assistance in administration of your estate.</p>
                                </li>
                                <li>
                                    <p class="justify">You may also appoint a professional executor of your estate, such as a legal firm.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back8" name="back8" data-back="8">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next10" name="next10" data-next="10">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 9 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page9 -->

        <div id="page10" class="page hidden">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>6. Professional trustees and executors </h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">There are advantages to appointing a professional executor, some of these include:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">saving your family having to make many decisions in relation to your affairs as much of the work will be done for them;</p>
                                </li>
                                <li>
                                    <p class="justify">a professional executor will have the necessary skills to finalise your estate quickly;</p>
                                </li>
                                <li>
                                    <p class="justify">a professional executor will be impartial and also independent if a dispute between family or otherwise arises.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>7. Public Trustee and Trustee Companies</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">A Trustee company can include a bank.  They may offer Wills that will appoint them as your executor.  Upon your death they will administer your estate but they will deduct a fee.  Usually the fee is based on the gross value of the assets of your estate.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">The Public Trustee prepares Wills appointing itself as executor and also Wills nominating private executors and charges a fee for this service. It offers a substantial discount on the fee if you appoint it as your executor. When you die, if the Public Trustee is your executor, it charges for estate administration on a fee for service basis.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">You may wish to consider our firm if you decide to appoint a professional executor.  We can act in and administer your estate independently and professionally.  We can also act as your executor jointly with a member of your family or a trusted friend/relative.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">I have read and understood the above information in relation to my new will</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Question 1</h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Full name</label></th>
                                            <th class="text-center"><label for="">Address</label></th>
                                            <th class="text-center"><label for="">Relationship to you</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for="">Executor/s</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_name_1" name="executor_name_1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="executor_address1" name="executor_address1" cols="30" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_relation1" name="executor_relation1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_name_2" name="executor_name_2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="executor_address2" name="executor_address2" cols="30" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_relation2" name="executor_relation2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">If there is more than one, do they have to act jointly?</label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="jointly1" name="jointly1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Alternative Executor/s</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_name_3" name="executor_name_3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="executor_address3" name="executor_address3" cols="30" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_relation3" name="executor_relation3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_name_4" name="executor_name_4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="executor_address4" name="executor_address4" cols="30" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="executor_relation4" name="executor_relation4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td class="">
                                                <label for="">If there is more than one, do they have to act jointly?</label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="jointly2" name="jointly2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div> 
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back9" name="back9" data-back="9">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next11" name="next11" data-next="11">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 10 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page10 -->

        <div id="page11" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Question 2</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Giving a specific gift in your Will </h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Unless it is your wish to leave a specific gift in your Will to someone other than your main beneficiaries, which include your spouse and children, it usually is unnecessary and may be inadvisable to give specific gifts because:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">your assets will change between making your Will and your death; and</p>
                                </li>
                                <li>
                                    <p class="justify">in particular, personal possessions can be destroyed, disposed of or lost prior to your death.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If your Will leaves your estate to your children equally your executor may:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">permit your children to take, as part of their entitlements, specific personal possessions; or</p>
                                </li>
                                <li>
                                    <p class="justify">sell any or all of your personal possessions and divide the proceeds among your children.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Description</label></th>
                                            <th class="text-center"><label for="">To Whom (full name)</label></th>
                                            <th class="text-center"><label for="">Relationship to you</label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for="">Any specific gifts of jewellery or personal effects?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2a1" name="question2a1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2b1" name="question2b1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2c1" name="question2c1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2a2" name="question2a2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2b2" name="question2b2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2c2" name="question2c2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2a3" name="question2a3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2b3" name="question2b3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2c3" name="question2c3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2a4" name="question2a4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2b4" name="question2b4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question2c4" name="question2c4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Question 3</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Leaving the remainder of your estate to your children</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">In the event your spouse has not survived you, it is usual that you leave your estate to your children in equal shares.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Your “children’ include any child for whom you are a natural parent including:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">any child or children you may have in the future;</p>
                                </li>
                                <li>
                                    <p class="justify">any adopted or illegitimate children; and</p>
                                </li>
                                <li>
                                    <p class="justify">any child or children from a previous marriage;</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">the term ‘children’ does not include step-children.</p>
                        </div>
                    </div>

                    <div class="row space">
                        <div class="col-sm-12">
                            <p class="justify">In the event you were to die leaving an infant child or children, then your executor will have the powers to provide for that child or children’s maintenance, education, welfare, advancement in life and benefit from their share of your estate.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>If any of your children die before you</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If any of your children die before you, you can choose to pass the share their parent would have received as follows:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    <p class="justify">to your children who do survive you;</p>
                                </li>
                                <li>
                                    <p class="justify">to your children’s children– i.e. your grandchildren – in equal shares; or</p>
                                </li>
                                <li>
                                    <p class="justify">your deceased child’s spouse or another relative.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back10" name="back10" data-back="10">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next12" name="next12" data-next="12">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 11 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page11 -->

        <div id="page12" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <h4>What to do if you do not wish to leave your estate to your children and/or grandchildren</h4>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">In the event you:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li>
                            <p class="justify">do not have any children; or</p>
                        </li>
                        <li>
                            <p class="justify">if you wish to leave your estate to your children unequally; or</p>
                        </li>
                        <li>
                            <p class="justify">if you wish to indicate in your Will who should share your estate in the event that all of your children (and grandchildren if applicable) were to die before you;</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">then you must list in your Will the full names of your beneficiaries and the percentage of the share you wish each of them to take.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">If, in this instance, any beneficiary named were to die before you then his or her share will be divided pro rata between any of the surviving beneficiaries.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">In the instance you are leaving your estate unequally to your children, it may be advisable to obtain specific advice on the possibility of your Will being challenged under the Inheritance (Family and Dependants Provision) Act.</p>
                </div>
            </div>

            <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><label for=""></label></th>
                                            <th class="text-center"><label for="">Full Name</label></th>
                                            <th class="text-center"><label for="">Relationship to you</label></th>
                                            <th class="text-center"><label for="">Legacy or Share </label></th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for="">Primary Beneficiaries</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a1" name="question3a1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b1" name="question3b1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c1" name="question3c1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a2" name="question3a2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b2" name="question3b2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c2" name="question3c2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Who are you Alternative Primary  Beneficiaries?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a3" name="question3a3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b3" name="question3b3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c3" name="question3c3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a4" name="question3a4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b4" name="question3b4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c4" name="question3c4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a5" name="question3a5">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b5" name="question3b5">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c5" name="question3c5">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a6" name="question3a6">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b6" name="question3b6">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c6" name="question3c6">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a7" name="question3a7">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b7" name="question3b7">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c7" name="question3c7">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">What age are beneficiaries to take their entitlement? <br> (i.e. 18, 21, 25) It is otherwise held on trust by your executor/s</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3a8" name="question3a8" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3b8" name="question3b8" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3c8" name="question3c8" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Who are your Further Alternative Primary  Beneficiaries? <br>
                                                    i.e. your grandchildren, any child or children of your <br> Alternative Beneficiaries in equal share etc.
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3a9" name="question3a9" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3b9" name="question3b9" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3c9" name="question3c9" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                    Do you want to be buried or cremated? <br> Any special conditions?
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3a10" name="question3a10" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3b10" name="question3b10" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3c10" name="question3c10" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                    Do you want to specifically exclude someone ?
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a11" name="question3a11">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b11" name="question3b11">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c11" name="question3c11">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a12" name="question3a12">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b12" name="question3b12">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c12" name="question3c12">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3a13" name="question3a13">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3b13" name="question3b13">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question3c13" name="question3c13">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">
                                                    Any other details or provisions?
                                                </label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3a14" name="question3a14" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3b14" name="question3b14" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="question3c14" name="question3c14" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back11" name="back11" data-back="11">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next13" name="next13" data-next="13">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 12 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page12 -->

        <div id="page13" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Question 4</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Naming a guardian for any of your infant children</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">If you have custody of any infant children you may wish to appoint in your Will a guardian for those infant children.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">The appointment of guardian is however subject to challenge on the basis that it may not in the children’s best interests following your death.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">You may wish to nominate your executor.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Full Name</label>
                                            </td>
                                            <td class="">
                                                <label for="">Relationship to you</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Guardian 1</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4a1" name="question4a1">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4b1" name="question4b1">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Address</label>
                                            </td>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4a2" name="question4a2">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4b2" name="question4b2">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Full Name</label>
                                            </td>
                                            <td class="">
                                                <label for="">Relationship to you</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Guardian 2</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4a3" name="question4a3">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4b3" name="question4b3">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Address</label>
                                            </td>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4a4" name="question4a4">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="question4b4" name="question4b4">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Can your Will be challenged after you die?</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Although you are entitled in your Will to dispose of your assets as you wish, you need to be aware that under the Inheritance (Family and Dependants Provision) Act, the provisions of your Will and distribution of your estate could be challenged if you fail to make adequate provision for certain family members.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Family members who may be entitled to challenge your Will include spouses, de facto spouses, former spouses who are in receipt of maintenance, your children and any dependant grandchildren.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Any person challenging your Will and making the claim must show that your estate did not make adequate provision for their proper maintenance, welfare, support, education or advancement in life.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Every application is examined by the Supreme Court on its merits.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Generally the Supreme Court is reluctant to rule against wishes that are expressed in your Will unless the person making the claim can show some need for your inheritance over and above the needs of the beneficiaries in your Will.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>What happens to your old Will?</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">You can make a new Will at any time. A new Will makes your previous Will invalid. You should discuss with your solicitor whether to retain or destroy your old Will. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">We recommend that you review your Will from time to time or when your circumstances change.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back12" name="back12" data-back="12">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next14" name="next14" data-next="14">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 13 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page13 -->

        <div id="page14" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3><u>PART 3</u></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Instructions for your new Power of Attorney and/or Enduring Power of Guardianship</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="justify">Do you require a Power of Attorney or an Enduring Power of Guardianship Yes / No</p>
                            <p class="justify">If yes</p>
                            <hr>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="bold">Power of Attorney</h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Full Name</label>
                                            </td>
                                            <td class="">
                                                <label for="">Address</label>
                                            </td>
                                            <td class="">
                                                <label for="">Relationship to you</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Who do you want to appoint as your Attorney/s?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a11" name="part3a11">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b11" name="part3b11">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c11" name="part3c11">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a12" name="part3a12">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b12" name="part3b12">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c12" name="part3c12">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="">If there is more than one, do they have to act jointly?</label></td>
                                            <td colspan="3">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="part3a131" name="part3a13">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="part3a132" name="part3a13">N
                                                </label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Full Name</label>
                                            </td>
                                            <td class="">
                                                <label for="">Address</label>
                                            </td>
                                            <td class="">
                                                <label for="">Relationship to you</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Who do you want to appoint as your Alternative attorney/s <br> to act if your attorney/s is unwilling or unable?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a14" name="part3a14">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b14" name="part3b14">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c14" name="part3c14">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a15" name="part3a15">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b15" name="part3b15">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c15" name="part3c15">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="">
                                                <label for="">If there is more than one, <br> do they have to act jointly?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a16" name="part3a16">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b16" name="part3b16">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c16" name="part3c16">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Do you want your attorney to be able to use their power <br> to benefit themselves or anyone other than you?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a17" name="part3a17">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b17" name="part3b17">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c17" name="part3c17">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">&nbsp;</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Do you want your Power of Attorney to be “Enduring” in nature? <br> i.e. to continue to be in effect <br> even if you lose mental capacity.</label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="part3a18" name="part3a18" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Do you want any restrictions on the actions <br> your Attorney/s can take or how long the power is in place?</label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="part3a19" name="part3a19" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Any other instructions?</label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="part3a110" name="part3a110" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div>
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back13" name="back13" data-back="13">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next15" name="next15" data-next="15">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 14 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page14 -->

        <div id="page15" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="bold">Enduring Power of Guardianship</h4>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-12">
                             <div class="table-responsive">
                                 <table class="table table-hover table-bordered">
                                    
                                    <tbody id="">
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Full Name</label>
                                            </td>
                                            <td class="">
                                                <label for="">Address</label>
                                            </td>
                                            <td class="">
                                                <label for="">Relationship to you</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Who do you want to appoint as your Guardian/s?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a21" name="part3a21">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b21" name="part3b21">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c21" name="part3c21">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a22" name="part3a22">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b22" name="part3b22">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c22" name="part3c22">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><label for="">If there is more than one, do they have to act jointly?</label></td>
                                            <td colspan="3">
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="1" id="part3a23y" name="part3a23">Y
                                                </label>
                                                <label class="checkbox-inline bold">
                                                    <input type="checkbox" value="0" id="part3a23n" name="part3a23">N
                                                </label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td class="">
                                                <label for="">Full Name</label>
                                            </td>
                                            <td class="">
                                                <label for="">Address</label>
                                            </td>
                                            <td class="">
                                                <label for="">Relationship to you</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Who do you want to appoint as your <br> Alternative guardian/s? To act if your guardian/s is unwilling or unable?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a24" name="part3a24">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b24" name="part3b24">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c24" name="part3c24">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for=""></label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a25" name="part3a25">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b25" name="part3b25">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c25" name="part3c25">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="">
                                                <label for="">If there is more than one, do they have to act jointly?</label>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3a26" name="part3a26">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3b26" name="part3b26">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="part3c26" name="part3c26">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Do you want to include any specific directions <br> regarding health decisions?</label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-sm" id="part3a27" name="part3a27" cols="3" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">
                                                <label for="">Do you want to include this standard direction <br> to your guardian: </label>
                                            </td>
                                            <td colspan="3">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <p class="justofy">
                                                            If I am so seriously ill that there is no likelihood of recovery then I direct my enduring guardian to ensure that I am not subjected to medical intervention unlikely to meaningfully prolong my life such as life support systems, artificial ventilation, blood transfusion, dialysis, antibiotics to control infection, artificial nutrition and surgery. It is my wish that I be kept comfortable, receive ordinary medical treatment and palliative care and be allowed to die. Any distressing symptoms should be controlled despite the necessary treatment shortening my life
                                                        </p>
                                                        <label class="checkbox-inline bold">
                                                            <input type="checkbox" value="1" id="part3a28y" name="part3a28">Y
                                                        </label>
                                                        <label class="checkbox-inline bold">
                                                            <input type="checkbox" value="0" id="part3a28n" name="part3a28">N
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                 </table>
                             </div>
                         </div>
                    </div>
                </div>
            </div>

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back14" name="back14" data-back="14">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next16" name="next16" data-next="16">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 15 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page15 -->

        <div id="page16" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="bold">Do I need an initial consultation?</h4>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">A short initial consultation costs <input type="text" id="consultation_costs" name="consultation_costs" class="input-sm" data-format="$ 0,0[.]00"> (plus GST) and takes about 20-30 minutes.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">At the consultation we will discuss your requirements and give you a fixed price quote for preparation of your Will.</p>
                    <p class="justify">If :</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li>
                            <p>you feel more comfortable discussing your Will in person with one of our lawyers;</p>
                        </li>
                        <li>
                            <p>we find that your affairs are more complex; or</p>
                        </li>
                        <li>
                            <p>if you were to require a Will on an urgent basis.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">then we suggest that you telephone our office on (02) 4229 8805 to arrange an initial consultation.</p>
                    <p class="justify">It is recommended that you seek an initial consultation if you:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li>
                            <p class="justify">wish to leave long lists of specific gifts;</p>
                        </li>
                        <li>
                            <p class="justify">wish to leave a gift to charity;</p>
                        </li>
                        <li>
                            <p class="justify">have an interest in a family trust, business, partnership or family company;</p>
                        </li>
                        <li>
                            <p class="justify">have a self-managed superannuation fund;</p>
                        </li>
                        <li>
                            <p class="justify">think that there is a possibility of your Will being challenged under the Inheritance (Family and Dependants Provision) Act;</p>
                        </li>
                        <li>
                            <p class="justify">intend to leave your estate to anyone other than your spouse or de facto spouse, children or other dependants;</p>
                        </li>
                        <li>
                            <p class="justify">do not have an understanding of the English language;</p>
                        </li>
                        <li>
                            <p class="justify">have difficulty understanding these instructions;</p>
                        </li>
                        <li>
                            <p class="justify">are illiterate;</p>
                        </li>
                        <li>
                            <p class="justify">are unable to sign your name;</p>
                        </li>
                        <li>
                            <p class="justify">want to bequeath your property with certain conditions attached – for example rights to purchase or life estate interests; or</p>
                        </li>
                        <li>
                            <p class="justify">require estate planning information including capital gains or other tax liabilities.</p>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back15" name="back15" data-back="15">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next17" name="next17" data-next="17">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 16 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page16 -->

        <div id="page17" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">Do you consider that you need an initial consultation?</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li>
                            <p class="justify">We reserves the right to refuse to accept your instructions.</p>
                        </li>
                        <li>
                            <p class="justify">By signing the foot of this document, you acknowledge and agree that you have read and understood the contents of this document in its entirety before giving us your instructions.</p>
                        </li>
                        <li>
                            <p class="justify">We will prepare your Will as soon as possible upon receipt of your complete instructions, however preparation of your Will may take up to 28 days from receipt of your instructions to complete same.</p>
                        </li>
                        <li>
                            <p class="justify">If you require an urgent Will you should arrange an initial consultation.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="bold">Completion of this document</h4>
                    <hr>    
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">Please return this completed document to us by:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li>
                            <p class="justify">•    Emailing it to  : info@stubbslaw.com.au;</p>
                        </li>
                        <li>
                            <p class="justify">•    Faxing it to  : (02) 4228 5475; or</p>
                        </li>
                        <li>
                            <p class="justify">•    Posting it to us at : PO Box 5431 Wollongong NSW 2500</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">Upon receipt of your completed form and payment we will:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul>
                        <li>
                            <p class="justify">Telephone you to clarify your instructions; and</p>
                        </li>
                        <li>
                            <p class="justify">Arrange to prepare your Will and mail or email it to you with instructions for signing and safe keeping (in the event that you do not wish to attend our office to sign your Will or for us to retain it for you).</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">You can elect to sign your Will at home or call into our office and sign it here. There is no charge for this service.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">You are also welcome to store the completed Will in our safe custody storage facility. There is no charge to you if you wish for us to store your Will; however there may be a small administration charge for returning the Will to you upon your request.</p>
                </div>
            </div>


            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back16" name="back16" data-back="16">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary next" id="next18" name="next18" data-next="18">NEXT</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 17 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page17 -->

        <div id="page18" class="page hidden">
            
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="bold">Acknowledgment</h4>
                    <hr>
                </div>
            </div>
            <div class="row space">
                <div class="col-sm-12 text-center">
                    <h4 class="bold">Client Acknowledgment</h4>
                </div>
            </div>
            <div class="row space">
                <div class="col-sm-12">
                    <p class="justify">The information provided in this Instruction Sheet is complete and accurate to the best of my knowledge (except where I have indicated that I have chosen not to provide the information).</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">I understand and acknowledge that by either not fully or accurately completing the Instruction Sheet any recommendation or advice given by Rachel Stubbs & Associates may be inappropriate to my individual needs.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="client" class="col-sm-4 control-label pull_left">Client signature:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control input-md" id="client_sign" name="client_sign">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="client" class="col-sm-4 control-label pull_left">Client name:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control input-md" id="client_name" name="client_name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="client" class="col-sm-4 control-label pull_left">Date:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control input-md datepicker" id="date_client" name="date_client">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <p class="justify">Please contact reception for assistance on (02) 4229 8805 if you have any questions.</p>
                </div>
            </div>
            

            <div class="row space">
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-lg btn-primary back" id="back17" name="back17" data-back="17">BACK</button>
               </div>
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-lg btn-primary" id="btn_pdf" name="btn_pdf">Download PDF</button>
                </div>
            </div>
            <div class="row page_foot">
                <div class5"col-md-15">
                    <p class="text-right bold">
                        (Page 18 of 18)
                    </p>
                </div>
            </div>
        </div><!-- close page18 -->

                <div class="modal fade" id="ask_question">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title">Please Submit Your Questions Here</h5>
                            </div>
            
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <textarea type="text" class="form-control input-sm" id="ask1" name="ask1" cols="3" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-warning reset" data-dismiss="modal">Cancel</button>
                              <button id="submit_question" type="button" name="submit_question" value="submit_question" class="btn btn-success">Send</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
    </form>
</div>



<script src="asset/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="asset/js/jquery-calx-1.1.9.js" type="text/javascript"></script>
<script src="asset/js/bootstrap.min.js" type="text/javascript"></script>
<script src="asset/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="asset/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="asset/js/brendan2.js" type="text/javascript"></script>
</body>
</html>