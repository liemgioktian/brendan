<?php 
foreach ($_POST as $key => $value) {
    $$key = htmlentities($value);
}
?>
 
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="content-type" content="text/html" />
    <meta name="author" content="4121" />
    <title>PDF Result</title>
    <style>
        body{
            padding:0;
            margin:0;
            font-family: Tahoma, Arial, Helvetica, Tahoma, serif;
            color:#000;
        }
        @page{
            margin-top:0;
            margin-left: 0.5cm;
            margin-bottom:0;
            margin-right:0.5cm;
        }
        .page{
            /*height:27.9cm;*/
            width:19cm;
            margin:0 auto;
            page-break-after:auto;
            overflow:auto;
            font-size:100%;
            background:white;
            padding:10px;
        }
        .pagebreak{
            page-break-after:always;
            
        }
        .center{
            text-align:center;
        }
        .bold{
            font-weight: bold;
        }
        .italic{
            font-style: italic;
        }
        
        p{
            text-align:justify;
            margin:2px 0;
        }
        div.content{
            padding:10px;
        }
        table{
            border-collapse:collapse;
        }
        td.box{
            border:solid 1px #000;
        }
        td.grid{
            width: <?php echo 100/12 ?>%;
            padding: 3px;

        }
        td.grid-small{
            width: <?php echo 100/12 ?>%;
            padding-top: 3px;

        }
        td,p,body,li{
            font-size:13px;
        }
        .small{
            font-size: 10px;
        }
        .linespace{
            line-height: 20px;
            text-align:justify;
        }
        td.underline{
            border-bottom: solid 1px black;
        }
        td.text-line{
           
            text-decoration:underline;
        }
        td.dotted{
           border-bottom: dotted .5px black;
            text-decoration:none;
        }
        .tops{
            margin-top: 10px;
        }
        .top{
            margin-top: 20px;
        }
        .right{
            text-align:right;
            padding-right:3px;
        }
        .bottom{
            margin-bottom: 20px;
        }
        .box{
            background: #dedede;
            padding : 5px;
        }
        .justify{
            text-align: justify;
        }
        .border{
            border: solid 1px grey;
            padding: 3px;
        }
        .border-bottom{
            border-bottom: solid 1px grey;
            padding: 3px;
        }
        .border-top{
            border-top: solid 1px grey;
            padding: 3px;
        }
        .border-right{
            border-right: solid 1px grey;
            padding: 3px;
        }
        .border-left{
            border-left: solid 1px grey;
            padding: 3px;
        }
        .zero{
            height: 0px;
        }
        .less{
            margin-top:0px;
            margin-bottom:0px;
        }
        .bless{
            margin-bottom: 0px;
        }
        .input{
            padding:3px;
            background-color: #fff;
        }
        .red{
            color: #C7203C;
        }
        .footer { position: fixed; bottom: 10px; }
    </style>
</head>
<body>
<!-- page1 -->
<div class="page">

    <div id="cover_page" class="content" style="padding:5px;padding-top:150px">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
           
            <tr style="margin-top:100px">
                <td class="grid bold" style="border-right:solid 2px #000" colspan="4">
                    &nbsp;
                </td>
                <td class="grid bold center" colspan="8" >
                    <img class="img" src='asset/img/rachel_logo.png' style="width:225px;margin-top:50px" alt="Will Instruction Form">
                    <p class="" style="font-size:40px">Will Instruction Form</p>
                </td>
            </tr>
            <tr>
                <td class="grid bold" style="border-right:solid 2px #000" colspan="4">
                    &nbsp;
                </td>
                <td class="grid bold" colspan="2"><label for="">Client</label></td>
                <td class="grid bold" colspan="1"><label for="">:</label></td>
                <td class="grid bold center" colspan="5"><p class="input">&nbsp;<?php echo $client ?></p></td></td>
            </tr>
            <tr>
                <td class="grid bold" style="border-right:solid 2px #000" colspan="4">
                    &nbsp;
                </td>
                <td class="grid bold" colspan="2"><label for="">Date sheet issued</label></td>
                <td class="grid bold" colspan="1"><label for="">:</label></td>
                <td class="grid bold center" colspan="5"><p class="input">&nbsp;<?php echo $date_issued ?></p></td></td>
            </tr>
            
        </table>
       
    </div>
    <div class="pagebreak"></div>

    <div id="page1" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Introduction</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12"><p class="justify linespace">The information requested in this Instruction Sheet is necessary to enable us to properly review and advise you in relation to your new or updated Will and/or Power of Attorney and/or Enduring Power of Guardianship. It's really important to be honest and include all of the details requested if they apply to you. If you don't provide all of the information requested by us this may result in us drafting documents for you which do not appropriately address your individual needs and which may result in unwanted results during your lifetime or after you pass away. </p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12"><p class="justify linespace">We confirm all information supplied will remain private and confidential at all times.  We will not disclose any client information to a third party unless instructed to do so or where required by law.</p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12"><p class="justify linespace">There are three (3) parts to this Instruction Sheet:</p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <ul class="less">
                        <li><p class="justify">Part 1: Asks questions about your personal situation, who you are, who's in your family and your assets and liabilities;</p></li>
                        <li><p class="justify">Part 2: Asks for your instructions for your new Will, who you want to be your executor, beneficiaries etc; and</p></li>
                        <li><p class="justify">Part 3: Asks for your instructions for your new Power of Attorney and/or Enduring Power of Guardianship.</p></li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12"><p class="justify">We ask that you please full complete and return the Instruction Sheet to us as soon as possible so that we can commence drafting your Will. </p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12"><h4 class="justify linespace"><u>Initial Details</u></h4></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6">&nbsp;</td>
                <td class="grid bold border center" colspan="6"><label for="">Client</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">1. Do you have a Will?</label></td>
                <td class="grid bold border center" colspan="6">
                    <img src="asset/images/<?php echo (isset($client1) && $client1 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($client1) && $client1 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N  ( if no go to Q 4) 
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">2. When was it last reviewed?</label></td>
                <td class="grid bold border center" colspan="6"><p class="input">&nbsp;<?php echo $reviewed ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">3. Where is it located?</label></td>
                <td class="grid bold border center" colspan="6">
                    <p class="input">solicitors name :&nbsp;<?php echo $solicitor ?></p>
                    <p class="input">firm name :&nbsp;<?php echo $firm ?></p>
                    <p class="input">firm address :&nbsp;<?php echo $address1 ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for=""></label></td>
                <td class="grid bold border center" colspan="6">
                    <label for="">Do you authorize us the retrieve it 
                    <img src="asset/images/<?php echo (isset($client2) && $client2 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($client2) && $client2 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                    </label>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">4. Do you have an Enduring Power of Attorney?</label></td>
                <td class="grid bold border center" colspan="6">
                    <img src="asset/images/<?php echo (isset($client3) && $client3 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($client3) && $client3 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">5. Do you have an Enduring Power of Guardianship?</label></td>
                <td class="grid bold border center" colspan="6">
                    <img src="asset/images/<?php echo (isset($client4) && $client4 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($client4) && $client4 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12"><p class="justify linespace">If you answered yes to any of the above questions please provide us with a copy of the document /s, if available. </p></td>
            </tr>
        </table>

        <div class="footer right" colspan="12">
            <p class="right bold">(Page 1 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page2" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3><u>PART 1</u></h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="less">Personal Details</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="less"><u>Basic Information</u></h4>
                </td>
            </tr>
            <tr class="bless">
                <td class="grid bold border"colspan="3">&nbsp;</td>
                <td class="grid bold border center" colspan="9"><label for="">Client</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Title</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $title ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Surname</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $surname ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Given Names</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $given_names ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Date of Birth</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $dob ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Sex</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $sex ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Marital Status</label></td>
                <td class="grid bold border center" colspan="9">
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Married
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '2') ? '1' : '0' ?>.gif" alt="">&nbsp; Single 
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '3') ? '1' : '0' ?>.gif" alt="">&nbsp; Divorced
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '4') ? '1' : '0' ?>.gif" alt="">&nbsp; Widowed 
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '5') ? '1' : '0' ?>.gif" alt="">&nbsp; Defacto 
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '6') ? '1' : '0' ?>.gif" alt="">&nbsp; Separated 
                    <br>
                    <img src="asset/images/<?php echo (isset($marital) && $marital == '7') ? '1' : '0' ?>.gif" alt="">&nbsp; Same Sex Domestic Relationship 
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Current Partners Name</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $partner_name ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Relationship commenced</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $commenced ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Residential Address</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $res_address ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Postal Address if different</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $postal_address ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless"><u>Basic Information</u></h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Contact Numbers</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">H : &nbsp;<?php echo $hoem_phone ?></p>
                    <p class="input">M : &nbsp;<?php echo $mobile_phone ?></p>
                    <p class="input">W : &nbsp;<?php echo $work_phone ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Fax</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $fax ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Email</label></td>
                <td class="grid bold border center" colspan="9">
                    <p class="input">&nbsp;<?php echo $email ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless"><u>Children</u></h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border center" colspan="3"><label for="">Name</label></td>
                <td class="grid bold border center" colspan="2"><label for="">Mutual Child</label></td>
                <td class="grid bold border center" colspan="2"><label for="">Age</label></td>
                <td class="grid bold border center" colspan="2"><label for="">Financially Dependent</label></td>
                <td class="grid bold border center" colspan="3"><label for="">Address</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $name_child1 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($mutual1) && $mutual1 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($mutual1) && $mutual1 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $age_child1 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($financial_depend1) && $financial_depend1 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($financial_depend1) && $financial_depend1 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $address_child1 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $name_child2 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($mutual2) && $mutual2 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($mutual2) && $mutual2 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $age_child2 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($financial_depend2) && $financial_depend2 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($financial_depend2) && $financial_depend2 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $address_child2 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $name_child3 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($mutual3) && $mutual3 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($mutual3) && $mutual3 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $age_child3 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($financial_depend3) && $financial_depend3 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($financial_depend3) && $financial_depend3 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $address_child3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $name_child4 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($mutual4) && $mutual4 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($mutual4) && $mutual4 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $age_child4 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($financial_depend4) && $financial_depend4 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($financial_depend4) && $financial_depend4 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $address_child4 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $name_child5 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($mutual5) && $mutual5 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($mutual5) && $mutual5 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $age_child5 ?></p></td>
                <td class="grid bold border center"colspan="2">
                    <img src="asset/images/<?php echo (isset($financial_depend5) && $financial_depend5 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($financial_depend5) && $financial_depend5 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $address_child5 ?></p></td>
            </tr>
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 2 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page3" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3 class="bless">Other Dependants</h3>
                </td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="4"><label>Name</label></td>
                <td class="grid bold border" colspan="4"><label>Relationship to you</label></td>
                <td class="grid bold border" colspan="4"><label>Age</label></td>
            </tr>
            <?php for($index=1; $index<=$count_dependants; $index++): ?>
            <tr class="">
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "name_depend$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "relation_depend$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "address_depend$index"; echo $$var ?></p></td>
            </tr>
            <?php endfor; ?>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Former Relationships</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6">&nbsp;</td>
                <td class="grid bold border center" colspan="6"><label for="">Client</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Have you been previously married or in a de facto relationship (2 years or more cohabiting or sharing finances to some extent)? <br> If yes, please complete:</label></td>
                <td class="grid bold border center"colspan="6">
                    <img src="asset/images/<?php echo (isset($former1) && $former1 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($former1) && $former1 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N (if no go to employment details)
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Former partner's name</label></td>
                <td class="grid bold border center"colspan="6"><p class="input">&nbsp;<?php echo $former_name ?></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Relationship duration</label></td>
                <td class="grid bold border center"colspan="6"><p class="input">&nbsp;<?php echo $relation_dur ?></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">When did the relationship end?</label></td>
                <td class="grid bold border center"colspan="6"><p class="input">&nbsp;<?php echo $relation_end ?></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Have you been previously married or in a de facto relationship (2 years or more cohabiting or sharing finances to some extent)? <br> If yes, please complete:</label></td>
                <td class="grid bold border center"colspan="6">
                    <img src="asset/images/<?php echo (isset($former2) && $former2 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($former2) && $former2 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Where there any relationships prior this </label></td>
                <td class="grid bold border center"colspan="6">
                    <img src="asset/images/<?php echo (isset($former3) && $former3 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($former3) && $former3 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>

            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Employment Details</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6">&nbsp;</td>
                <td class="grid bold border center" colspan="6"><label for="">Client</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Have you been previously married or in a de facto relationship (2 years or more cohabiting or sharing finances to some extent)? <br> If yes, please complete:</label></td>
                <td class="grid bold border center"colspan="6">
                    <img src="asset/images/<?php echo (isset($employment) && $employment == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Employed 
                    <img src="asset/images/<?php echo (isset($employment) && $employment == '2') ? '1' : '0' ?>.gif" alt="">&nbsp; Self-employed 
                    <img src="asset/images/<?php echo (isset($employment) && $employment == '3') ? '1' : '0' ?>.gif" alt="">&nbsp; Unemployed 
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="4"><label for="">Occupation</label></td>
                <td class="grid bold border center"colspan="8"><p class="input">&nbsp;<?php echo $occupation ?></td>
            </tr>
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 3 of 8)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page4" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h3 class="bless">Financial Details</h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Income</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6">&nbsp;</td>
                <td class="grid bold border center" colspan="6"><label for="">Client</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Gross Annual Income</label></td>
                <td class="grid bold border" colspan="6"><p class="input">&nbsp;<?php echo $gross_income ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Are you receiving Social Security benefits?</label></td>
                <td class="grid bold border center" colspan="6">
                    <img src="asset/images/<?php echo (isset($income) && $income == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($income) && $income == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="6"><label for="">Any other income?</label></td>
                <td class="grid bold border center" colspan="6">
                    <img src="asset/images/<?php echo (isset($onther_income) && $onther_income == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($onther_income) && $onther_income == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless"><u>Assets</u></h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12"><label for="">Principal Residence</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="4"><label for="">Owner/s</label></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php echo $owners1 ?></p></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php echo $owners2 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="4"><label for="">Value</label></td>
                <td class="grid bold border" colspan="8"><p class="input">&nbsp;<?php echo $owners_value ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="4"><label for="">Mortgage to</label></td>
                <td class="grid bold border" colspan="8"><p class="input">&nbsp;<?php echo $owners_mortgage ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="4"><label for="">Amount owing on mortgage:</label></td>
                <td class="grid bold border" colspan="8"><p class="input">&nbsp;<?php echo $owners_owing ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="4"><label for="">Date Acquired</label></td>
                <td class="grid bold border" colspan="8"><p class="input">&nbsp;<?php echo $owners_acquired ?></p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless"><u>Other Assets</u></h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for=""></label></td>
                <td class="grid bold border"colspan="3"><label for="">Owner</label></td>
                <td class="grid bold border"colspan="3"><label for="">Asset Value</label></td>
                <td class="grid bold border"colspan="3"><label for="">Details</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Household Contents</label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $household1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $household2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $household3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Non-Income Producing Real Estate (eg. holiday home, land) </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $non_income1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $non_income2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $non_income3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Motor Vehicles </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $motor_vehicles1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $motor_vehicles2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $motor_vehicles3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Boat / Marine </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $boat1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $boat2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $boat3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Caravan </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $caravan1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $caravan2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $caravan3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Jewellery </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $jewellery1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $jewellery2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $jewellery3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Collectibles / Art / Other </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $art1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $art2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $art3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Business</label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $business1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $business2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $business3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border"colspan="3"><label for="">Others </label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $others1 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $others2 ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php echo $others3 ?></p></td>
            </tr>
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 4 of 8)</p>
        </div>
    </div>
    <div class="pagebreak"></div>
    
    <div id="page5" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h3 class="bless">Investment Assets</h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3 class="bless">Current Investments / Bank Accounts/ Savings (excluding investment properties)</h3>
                </td>
            </tr>
            <tr>
                <td class="grid bold border"></td>
                <td class="grid bold border" colspan="5"><label>Description / Account number </label></td>
                <td class="grid bold border" colspan="3"><label>Owner</label></td>
                <td class="grid bold border" colspan="3"><label>Current Value</label></td>
            </tr>
            <?php for($index=1; $index<=$count_invest_assets; $index++): ?>
            <tr class="">
                <td class="grid bold border"><label for=""><?= $index ?></label></td>
                <td class="grid bold border" colspan="5"><p class="input">&nbsp;<?php $var = "invest_account$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php $var = "invest_owner$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php $var = "invest_value$index"; echo $$var ?></p></td>
            </tr>
            <?php endfor; ?>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless"><u>Investment Property</u></h4>
                </td>
            </tr>
            <tr>
                <td class="grid bold border"></td>
                <td class="grid bold border" colspan="3"><label>Address</label></td>
                <td class="grid bold border" colspan="2"><label>Owner</label></td>
                <td class="grid bold border" colspan="2"><label>Date Purchased</label></td>
                <td class="grid bold border" colspan="2"><label>Value</label></td>
                <td class="grid bold border" colspan="2"><label>Current Loan Balance</label></td>
            </tr>
            <?php for($index=1; $index<=$count_invest_assets; $index++): ?>
            <tr class="">
                <td class="grid bold border"><label for=""><?= $index ?></label></td>
                <td class="grid bold border" colspan="3"><p class="input">&nbsp;<?php $var = "property_address$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="2"><p class="input">&nbsp;<?php $var = "property_owner$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="2"><p class="input">&nbsp;<?php $var = "property_date$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="2"><p class="input">&nbsp;<?php $var = "property_value$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="2"><p class="input">&nbsp;<?php $var = "property_balance$index"; echo $$var ?></p></td>
            </tr>
            <?php endfor; ?>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless"><u>Superannuation</u></h4>
                </td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="8"><label>&nbsp;</label></td>
                <td class="grid bold border" colspan="4"><label>&nbsp;</label></td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="8"><label>Fund Name</label></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php echo $fund_name1 ?></p></td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="8"><label>Current Account Balance</label></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php echo $super_balance1 ?></p></td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="8">
                    <label>
                        Death Benefit Nomination Details?
                        (Have you nominated the person who is to receive your super
                        balance and/or life insurance when you die?)
                    </label>
                </td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php echo $super_death1 ?></p></td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="8">
                    <label>
                        If you have a Death Benefit Nomination,
                        is it binding or non binding?
                    </label>
                </td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php echo $super_binding1 ?></p></td>
            </tr>
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 5 of 8)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page6" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h3 class="bless">Liabilities</h3>
                </td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="4"></td>
                <td class="grid bold border" colspan="4"><label>Current Loan Balance</label></td>
                <td class="grid bold border" colspan="4"><label>Bank / person responsible </label></td>
            </tr>
            <?php for($index=1; $index<=$count_liabilities; $index++): ?>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for=""><?= $index==1?"Principal Residence":"Investment Loan $index" ?></label></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "liabilitas_loan$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "liabilitas_bank$index"; echo $$var ?></p></td>
            </tr>
            <?php endfor; ?>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h3 class="bless">Liabilities other</h3>
                </td>
            </tr>
            <tr>
                <td class="grid bold border" colspan="4"></td>
                <td class="grid bold border" colspan="4"><label>Current Loan Balance</label></td>
                <td class="grid bold border" colspan="4"><label>Bank / person responsible </label></td>
            </tr>
            <?php for($index=1; $index<=$count_liabilities_other; $index++): ?>
            <tr class="">
                <?php
                 switch ($index){
                     case 1: $lia_title = 'Car Loan'; break;
                     case 2: $lia_title = 'Personal Loan'; break;
                     case 3: $lia_title = 'Credit Card'; break;
                     case 4: $lia_title = 'Other'; break;
                 }
                ?>
                <td class="grid bold border" colspan="4"><label for=""><?= $lia_title ?></label></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "lia_other_loan$index"; echo $$var ?></p></td>
                <td class="grid bold border" colspan="4"><p class="input">&nbsp;<?php $var = "lia_other_bank$index"; echo $$var ?></p></td>
            </tr>
            <?php endfor; ?>
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 6 of 8)</p>
        </div>
    </div>
    <div class="pagebreak"></div>
    
    <div id="page7" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid center" colspan="12">
                    <h3 class="bless">Other Assets</h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <p>If you have any of the following insurances, assets, entities or structures in place, please as much detail as possible in relation to these below:</p>
                    <ul>
                        <li><u>Insurances</u>: Life and TPD Cover, Income Protection Insurance, Critical Illness Cover, General Home, Contents or Vehicle insurance</li>
                        <li><u>Other Entities</u>: Self-Managed Superannuation Fund, Company, Partnership, Trust</li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid border" colspan="12">
                    <p><?= strlen($page7_1)>0?$page7_1:'&nbsp;' ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <p>Is there anything else which you would like us to know? Please let us have your comments.</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid border" colspan="12">
                    <p><?= strlen($page7_2)>0?$page7_2:'&nbsp;' ?></p>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <p>Acknowledgement: I have read and understood all questions the above</p>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <p>Don't understand anything - No problem ask us.</p>
                </td>
            </tr>
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 7 of 8)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page8" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3 class=""><u>PART 2</u></h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">1. Instructions for your new Will</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If your affairs are straightforward, we have designed this form in order to capture all appropriate information we need to make a Will for you.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Each person wanting a Will should complete one of these forms. For example, if both you and your spouse require Wills, please complete two forms.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">This form is not designed for people with complex affairs or people requiring a complex Will. If this is your situation, you should contact us for an initial consultation.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">This form is not designed for people with complex affairs or people requiring a complex Will. If this is your situation, you should contact us for an initial consultation.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If you otherwise have any questions about completing the form please telephone us on (02) 4229 8805 for assistance.</p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">2. Will any marriage, divorce or separation affect your Will?</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If you are in a defacto relationship or contemplating marriage, your Will should reflect these situations otherwise, a pre-existing Will is automatically revoked when you marry.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If you have divorced your existing Will may thereby revoked. If you have recently separated and you have a Will, chances are you have left everything to your former spouse. If you do not have a Will, the law assumes that he / she is your major beneficiary until you are divorced. In these circumstances you should consider making a new Will.</p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">3. Property that is not affected by a Will</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">In some instances, property that you regard as your own may not, upon your death, form part of your estate.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Upon your death if you own property as a joint tenant with any person, that property is not affected by your Will as it will pass to the surviving joint owner(s). Some examples are:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">Any real estate, for instance, the family home that you may own as a joint tenant with your spouse or another person;</p>
                        </li>
                        <li>
                            <p class="justify">Any jointly owned bank, building society or credit union accounts; and</p>
                        </li>
                        <li>
                            <p class="justify">Any jointly owned shares.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">In the event of your death, entitlements under a superannuation or life insurance policy where benefits are payable to a nominated beneficiary are also not affected by your Will.</p></td>
            </tr>

        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 8 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page9" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">4. The executor of your Will</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Your appointed executor is the person or persons nominated in your Will to administer your estate and carry out any of your wishes upon your death. Careful consideration should be given to choosing an executor.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">The duties of an executor include the following:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">making all necessary arrangements for your funeral;</p>
                        </li>
                        <li>
                            <p class="justify">paying any funeral or associated expenses and any debts of your estate;</p>
                        </li>
                        <li>
                            <p class="justify">Notifying all beneficiaries named in your Will and any interested parties</p>
                        </li>
                        <li>
                            <p class="justify">taking charge of and protecting your assets;</p>
                        </li>
                        <li>
                            <p class="justify">selling and/or distributing your assets according to the directions in your Will.</p>
                        </li>
                        <li>
                            <p class="justify">finalising your income tax returns; and</p>
                        </li>
                        <li>
                            <p class="justify">obtaining an authority to administer your estate in the form of a Grant of Probate from the Supreme Court if necessary;</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Any reasonable out of pocket expenses and costs incurred by your executor are usually paid out of your estate.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Generally your executor is also entitled to retain professional assistance if he or she considers it necessary.</p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">5. Who should you choose as an Executor.</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You should choose someone reliable that you trust. Any appointed executor is not bound to act for you and it is therefore a good idea to talk to them before appointing them. Your executor does not need any special qualifications but must be willing to undertake the duty appointed on them.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If your executor considers that he/she requires professional assistance they are at liberty to engage a lawyer or other suitably qualified professional following your death.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If you wish, you may appoint more than one executor. If you wish you can appoint one primary executor and then an alternate executor who is willing to undertake the duty if the primary executor is unable or unwilling to after your death.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You may appoint one of your beneficiaries named in your Will as your executor or executors.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">The choice of executor is your decision. The following may assist you in making that decision:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">If it is your intention to leave everything in your Will to one single person, it would be prudent to appoint that person as your executor.</p>
                        </li>
                        <li>
                            <p class="justify">If it is your intention to leave everything in your Will to a spouse, if they survive you, it would be prudent to appoint that spouse as your primary executor.</p>
                        </li>
                        <li>
                            <p class="justify">If it is your intention to leave everything in your Will to your children, in the event that you have no spouse, or, if they have predeceased you, then you may wish to consider appointing one or more of your children who are aged over 18 years of age as your executor or alternative executor.</p>
                        </li>
                        <li>
                            <p class="justify">If you wish, you may also appoint a trusted family friend or relative.</p>
                        </li>
                        <li>
                            <p class="justify">You may also wish to make a direction in your Will that your executor engage a solicitor to ensure that they obtain assistance in administration of your estate.</p>
                        </li>
                        <li>
                            <p class="justify">You may also appoint a professional executor of your estate, such as a legal firm.</p>
                        </li>
                    </ul>
                </td>
            </tr>

        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 9 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page10" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">6. Professional trustees and executors </h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">There are advantages to appointing a professional executor, some of these include:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">saving your family having to make many decisions in relation to your affairs as much of the work will be done for them;</p>
                        </li>
                        <li>
                            <p class="justify">a professional executor will have the necessary skills to finalise your estate quickly;</p>
                        </li>
                        <li>
                            <p class="justify">a professional executor will be impartial and also independent if a dispute between family or otherwise arises.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">7. Public Trustee and Trustee Companies</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">A Trustee company can include a bank. They may offer Wills that will appoint them as your executor. Upon your death they will administer your estate but they will deduct a fee. Usually the fee is based on the gross value of the assets of your estate.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">The Public Trustee prepares Wills appointing itself as executor and also Wills nominating private executors and charges a fee for this service. It offers a substantial discount on the fee if you appoint it as your executor. When you die, if the Public Trustee is your executor, it charges for estate administration on a fee for service basis.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You may wish to consider our firm if you decide to appoint a professional executor. We can act in and administer your estate independently and professionally. We can also act as your executor jointly with a member of your family or a trusted friend/relative.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">I have read and understood the above information in relation to my new will</p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless">Question 1</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for=""></label></td>
                <td class="grid bold border" colspan="3"><label for="">Full name</label></td>
                <td class="grid bold border" colspan="3"><label for="">Address</label></td>
                <td class="grid bold border" colspan="3"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for="">Executor/s</label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_name_1 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_address1 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_relation1 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for=""></label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_name_2 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_address2 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_relation2 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for="">If there is more than one, do they have to act jointly?</label></td>
                <td class="grid bold border"colspan="9"><p class="input">&nbsp;<?php echo $jointly1 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for="">Alternative Executor/s</label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_name_3 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_address3 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_relation3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for=""></label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_name_4 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_address4 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $executor_relation4 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="3"><label for="">If there is more than one, do they have to act jointly?</label></td>
                <td class="grid bold border"colspan="9"><p class="input">&nbsp;<?php echo $jointly2 ?></p></td>
            </tr>
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 10 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page11" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless">Question 2</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Giving a specific gift in your Will </h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Unless it is your wish to leave a specific gift in your Will to someone other than your main beneficiaries, which include your spouse and children, it usually is unnecessary and may be inadvisable to give specific gifts because:</p></td>
            </tr>
            <tr class="less">
                <td class="grid bold less" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">your assets will change between making your Will and your death; and</p>
                        </li>
                        <li>
                            <p class="justify">in particular, personal possessions can be destroyed, disposed of or lost prior to your death.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If your Will leaves your estate to your children equally your executor may:</p></td>
            </tr>
            <tr class="less">
                <td class="grid bold less" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">permit your children to take, as part of their entitlements, specific personal possessions; or</p>
                        </li>
                        <li>
                            <p class="justify">sell any or all of your personal possessions and divide the proceeds among your children.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border center" colspan="5"><label for=""></label></td>
                <td class="grid bold border center" colspan="3"><label for="">Description</label></td>
                <td class="grid bold border center" colspan="2"><label for="">To Whom (full name)</label></td>
                <td class="grid bold border center" colspan="2"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Any specific gifts of jewellery or personal effects?</label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question2a1 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2b1 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2c1 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question2a2 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2b2 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2c2 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question2a3 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2b3 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2c3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question2a4 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2b4 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question2c4 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless">Question 3</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Leaving the remainder of your estate to your children</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">In the event your spouse has not survived you, it is usual that you leave your estate to your children in equal shares.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify bless">Your 'children' include any child for whom you are a natural parent including:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">any child or children you may have in the future;</p>
                        </li>
                        <li>
                            <p class="justify">any adopted or illegitimate children; and</p>
                        </li>
                        <li>
                            <p class="justify">any child or children from a previous marriage;</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">the term 'children' does not include step-children.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">In the event you were to die leaving an infant child or children, then your executor will have the powers to provide for that child or children's maintenance, education, welfare, advancement in life and benefit from their share of your estate.</p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">If any of your children die before you</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If any of your children die before you, you can choose to pass the share their parent would have received as follows:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">to your children who do survive you;</p>
                        </li>
                        <li>
                            <p class="justify">to your children's children' i.e. your grandchildren ' in equal shares; or</p>
                        </li>
                        <li>
                            <p class="justify">your deceased child's spouse or another relative.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 11 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page12" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">What to do if you do not wish to leave your estate to your children and/or grandchildren</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">In the event you:</p></td>
            </tr>
            <tr class="less">
                <td class="grid bold less" colspan="12">
                    <ul class="less">
                        <li>
                            <p class="justify">do not have any children; or</p>
                        </li>
                        <li>
                            <p class="justify">if you wish to leave your estate to your children unequally; or</p>
                        </li>
                        <li>
                            <p class="justify">if you wish to indicate in your Will who should share your estate in the event that all of your children (and grandchildren if applicable) were to die before you;</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">then you must list in your Will the full names of your beneficiaries and the percentage of the share you wish each of them to take.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If, in this instance, any beneficiary named were to die before you then his or her share will be divided pro rata between any of the surviving beneficiaries.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">In the instance you are leaving your estate unequally to your children, it may be advisable to obtain specific advice on the possibility of your Will being challenged under the Inheritance (Family and Dependants Provision) Act.</p></td>
            </tr>
            
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border" colspan="2"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="2"><label for="">Relationship to you  </label></td>
                <td class="grid bold border" colspan="3"><label for="">Legacy or Share </label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Primary Beneficiaries</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a1 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b1 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c1 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a2 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b2 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c2 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Who are you Alternative Primary Beneficiaries?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a3 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b3 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a4 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b4 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c4 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a5 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b5 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c5 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a6 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b6 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c6 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a7 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b7 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c7 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">What age are beneficiaries to take their entitlement?(i.e. 18, 21, 25) It is otherwise held on trust by your executor/s</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a8 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b8 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c8 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Who are your Further Alternative Primary Beneficiaries? i.e. your grandchildren, any child or children of your Alternative Beneficiaries in equal share etc. </label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a9 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b9 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c9 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want to be buried or cremated? Any special conditions? </label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a10 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b10 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c10 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want to specifically exclude someone ? </label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a11 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b11 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c11 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a12 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b12 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c12 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a13 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b13 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c13 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Any other details or provisions? </label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3a14 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $question3b14 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $question3c14 ?></p></td>
            </tr>
            
            
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 12 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page13" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless">Question 4</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Naming a guardian for any of your infant children</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">If you have custody of any infant children you may wish to appoint in your Will a guardian for those infant children.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">The appointment of guardian is however subject to challenge on the basis that it may not in the children's best interests following your death.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You may wish to nominate your executor.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for=""></label></td>
                <td class="grid bold border" colspan="4"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="4"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for="">Guardian 1</label></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4a1 ?></p></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4b1 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for=""></label></td>
                <td class="grid bold border" colspan="4"><label for="">Address</label></td>
                <td class="grid bold border" colspan="4"><label for=""></label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for=""></label></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4a2 ?></p></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4b2 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for=""></label></td>
                <td class="grid bold border" colspan="4"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="4"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for="">Guardian 2</label></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4a3 ?></p></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4b3 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for=""></label></td>
                <td class="grid bold border" colspan="4"><label for="">Address</label></td>
                <td class="grid bold border" colspan="4"><label for=""></label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="4"><label for="">Guardian 2</label></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4a4 ?></p></td>
                <td class="grid bold border"colspan="4"><p class="input">&nbsp;<?php echo $question4b4 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Can your Will be challenged after you die?</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Although you are entitled in your Will to dispose of your assets as you wish, you need to be aware that under the Inheritance (Family and Dependants Provision) Act, the provisions of your Will and distribution of your estate could be challenged if you fail to make adequate provision for certain family members.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Family members who may be entitled to challenge your Will include spouses, de facto spouses, former spouses who are in receipt of maintenance, your children and any dependant grandchildren.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Any person challenging your Will and making the claim must show that your estate did not make adequate provision for their proper maintenance, welfare, support, education or advancement in life.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Every application is examined by the Supreme Court on its merits.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Generally the Supreme Court is reluctant to rule against wishes that are expressed in your Will unless the person making the claim can show some need for your inheritance over and above the needs of the beneficiaries in your Will.</p></td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">What happens to your old Will?</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You can make a new Will at any time. A new Will makes your previous Will invalid. You should discuss with your solicitor whether to retain or destroy your old Will.We recommend that you review your Will from time to time or when your circumstances change.</p></td>
            </tr>
            
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 13 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page14" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3 class="bless"><u>PART 3</u></h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Instructions for your new Power of Attorney and/or Enduring Power of Guardianship</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Do you require a Power of Attorney or an Enduring Power of Guardianship Yes / No</p></td>
            </tr>
            <tr class="">
                <td class="grid bold underline" colspan="12"><p class="justify">If yes</p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless"><u>Power of Attorney</u></h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border" colspan="2"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="3"><label for="">Address</label></td>
                <td class="grid bold border" colspan="2"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Who do you want to appoint as your Attorney/s?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a11 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b11 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c11 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a12 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b12 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c12 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">If there is more than one, do they have to act jointly?</label></td>
                <td class="grid bold border center" colspan="7">
                    <img src="asset/images/<?php echo (isset($part3a13) && $part3a13 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($part3a13) && $part3a13 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border" colspan="2"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="3"><label for="">Address</label></td>
                <td class="grid bold border" colspan="2"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Who do you want to appoint as your Alternative attorney/s to act if your attorney/s is unwilling or unable?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a14 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b14 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c14 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a15 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b15 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c15 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">If there is more than one, do they have to act jointly?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a16 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b16 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c16 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want your attorney to be able to use their power to benefit themselves or anyone other than you?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a17 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b17 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c17 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want your Power of Attorney to be 'Enduring' in nature? i.e. to continue to be in effect even if you lose mental capacity.</label></td>
                <td class="grid bold border"colspan="7"><p class="input">&nbsp;<?php echo $part3a18 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want any restrictions on the actions your Attorney/s can take or how long the power is in place?</label></td>
                <td class="grid bold border"colspan="7"><p class="input">&nbsp;<?php echo $part3a19 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Any other instructions?</label></td>
                <td class="grid bold border"colspan="7"><p class="input">&nbsp;<?php echo $part3a110 ?></p></td>
            </tr>
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 14 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page15" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3 class="bless"><u>PART 3</u></h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Instructions for your new Power of Attorney and/or Enduring Power of Guardianship</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Do you require a Power of Attorney or an Enduring Power of Guardianship Yes / No</p></td>
            </tr>
            <tr class="">
                <td class="grid bold underline" colspan="12"><p class="justify">If yes</p></td>
            </tr>
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h4 class="bless"><u>Power of Attorney</u></h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border" colspan="2"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="3"><label for="">Address</label></td>
                <td class="grid bold border" colspan="2"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Who do you want to appoint as your Attorney/s?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a11 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b11 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c11 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a12 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b12 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c12 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">If there is more than one, do they have to act jointly?</label></td>
                <td class="grid bold border center" colspan="7">
                    <img src="asset/images/<?php echo (isset($part3a13) && $part3a13 == '1') ? '1' : '0' ?>.gif" alt="">&nbsp; Y
                    <img src="asset/images/<?php echo (isset($part3a13) && $part3a13 == '0') ? '1' : '0' ?>.gif" alt="">&nbsp; N
                </td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border" colspan="2"><label for="">Full Name</label></td>
                <td class="grid bold border" colspan="3"><label for="">Address</label></td>
                <td class="grid bold border" colspan="2"><label for="">Relationship to you</label></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Who do you want to appoint as your Alternative attorney/s to act if your attorney/s is unwilling or unable?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a14 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b14 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c14 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for=""></label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a15 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b15 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c15 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">If there is more than one, do they have to act jointly?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a16 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b16 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c16 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want your attorney to be able to use their power to benefit themselves or anyone other than you?</label></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3a17 ?></p></td>
                <td class="grid bold border"colspan="3"><p class="input">&nbsp;<?php echo $part3b17 ?></p></td>
                <td class="grid bold border"colspan="2"><p class="input">&nbsp;<?php echo $part3c17 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want your Power of Attorney to be 'Enduring' in nature? i.e. to continue to be in effect even if you lose mental capacity.</label></td>
                <td class="grid bold border"colspan="7"><p class="input">&nbsp;<?php echo $part3a18 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Do you want any restrictions on the actions your Attorney/s can take or how long the power is in place?</label></td>
                <td class="grid bold border"colspan="7"><p class="input">&nbsp;<?php echo $part3a19 ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="5"><label for="">Any other instructions?</label></td>
                <td class="grid bold border"colspan="7"><p class="input">&nbsp;<?php echo $part3a110 ?></p></td>
            </tr>
            
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 15 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page16" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid justify" colspan="12">
                    <h3 class="bless">Do I need an initial consultation?</h3>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">A short initial consultation costs &nbsp;<?php echo $consultation_costs ?> (plus GST) and takes about 20-30 minutes.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">At the consultation we will discuss your requirements and give you a fixed price quote for preparation of your Will. If :</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul>
                        <li>
                            <p class="justify">you feel more comfortable discussing your Will in person with one of our lawyers;</p>
                        </li>
                        <li>
                            <p class="justify">we find that your affairs are more complex; or</p>
                        </li>
                        <li>
                            <p class="justify">if you were to require a Will on an urgent basis.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">then we suggest that you telephone our office on (02) 4229 8805 to arrange an initial consultation.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">It is recommended that you seek an initial consultation if you:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul>
                        <li>
                            <p class="justify">wish to leave long lists of specific gifts;</p>
                        </li>
                        <li>
                            <p class="justify">wish to leave a gift to charity;</p>
                        </li>
                        <li>
                            <p class="justify">have an interest in a family trust, business, partnership or family company;</p>
                        </li>
                        <li>
                            <p class="justify">have a self-managed superannuation fund;</p>
                        </li>
                        <li>
                            <p class="justify">think that there is a possibility of your Will being challenged under the Inheritance (Family and Dependants Provision) Act;</p>
                        </li>
                        <li>
                            <p class="justify">intend to leave your estate to anyone other than your spouse or de facto spouse, children or other dependants;</p>
                        </li>
                        <li>
                            <p class="justify">do not have an understanding of the English language;</p>
                        </li>
                        <li>
                            <p class="justify">have difficulty understanding these instructions;</p>
                        </li>
                        <li>
                            <p class="justify">are illiterate;</p>
                        </li>
                        <li>
                            <p class="justify">are unable to sign your name;</p>
                        </li>
                        <li>
                            <p class="justify">want to bequeath your property with certain conditions attached ' for example rights to purchase or life estate interests; or</p>
                        </li>
                        <li>
                            <p class="justify">require estate planning information including capital gains or other tax liabilities.</p>
                        </li>
                    </ul>
                </td>
            </tr>

        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 16 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page17" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Do you consider that you need an initial consultation?</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul>
                        <li>
                            <p class="justify">We reserves the right to refuse to accept your instructions.</p>
                        </li>
                        <li>
                            <p class="justify">By signing the foot of this document, you acknowledge and agree that you have read and understood the contents of this document in its entirety before giving us your instructions.</p>
                        </li>
                        <li>
                            <p class="justify">We will prepare your Will as soon as possible upon receipt of your complete instructions, however preparation of your Will may take up to 28 days from receipt of your instructions to complete same.</p>
                        </li>
                        <li>
                            <p class="justify">If you require an urgent Will you should arrange an initial consultation.</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold underline" colspan="12">
                    <h4 class="bless">Completion of this document</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Please return this completed document to us by:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul>
                        <li>
                            <p class="justify">Emailing it to : info@stubbslaw.com.au;</p>
                        </li>
                        <li>
                            <p class="justify">Faxing it to : (02) 4228 5475; or</p>
                        </li>
                        <li>
                            <p class="justify">Posting it to us at : PO Box 5431 Wollongong NSW 2500</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">Upon receipt of your completed form and payment we will:</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12">
                    <ul>
                        <li>
                            <p class="justify">Telephone you to clarify your instructions; and</p>
                        </li>
                        <li>
                            <p class="justify">Arrange to prepare your Will and mail or email it to you with instructions for signing and safe keeping (in the event that you do not wish to attend our office to sign your Will or for us to retain it for you).</p>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You can elect to sign your Will at home or call into our office and sign it here. There is no charge for this service.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify">You are also welcome to store the completed Will in our safe custody storage facility. There is no charge to you if you wish for us to store your Will; however there may be a small administration charge for returning the Will to you upon your request.</p></td>
            </tr>
            

        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 17 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

    <div id="page18" class="content">
        <table style="width:100%;">
            <tr>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
                <td class="grid zero">&nbsp;</td>
            </tr>
            <tr class="">
                <td class="grid justify underline" colspan="12">
                    <h4 class="bless">Acknowledgment</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid justify linespace center" colspan="12">
                    <h4 class="linespace">Acknowledgment</h4>
                </td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify linespace">The information provided in this Instruction Sheet is complete and accurate to the best of my knowledge (except where I have indicated that I have chosen not to provide the information).?</p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify linespace">I understand and acknowledge that by either not fully or accurately completing the Instruction Sheet any recommendation or advice given by Rachel Stubbs & Associates may be inappropriate to my individual needs.</p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="2"><label for="" class="linespace">Client signature</label></td>
                <td class="grid bold center" colspan="1"><label for="" class="linespace">:</td>
                <td class="grid bold border"colspan="9"><p class="input linespace">&nbsp;<?php echo $client_sign ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="2"><label for="" class="linespace">Client name</label></td>
                <td class="grid bold center" colspan="1"><label for="" class="linespace">:</td>
                <td class="grid bold border"colspan="9"><p class="input linespace">&nbsp;<?php echo $client_name ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold border" colspan="2"><label for="" class="linespace">Date</label></td>
                <td class="grid bold center" colspan="1"><label for="" class="linespace">:</td>
                <td class="grid bold border"colspan="9"><p class="input linespace">&nbsp;<?php echo $date_client ?></p></td>
            </tr>
            <tr class="">
                <td class="grid bold" colspan="12"><p class="justify linespace">Please contact reception for assistance on (02) 4229 8805 if you have any questions.</p></td>
            </tr>

           
        </table>
        <div class="footer right" colspan="12">
            <p class="right bold">(Page 18 of 18)</p>
        </div>
    </div>
    <div class="pagebreak"></div>

</div>
</body>
</html>
