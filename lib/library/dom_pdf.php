<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dom_pdf{
    private $_pdf;
    function __construct(){
        include 'dompdf/dompdf_config.inc.php';
        $this->_pdf=new DOMPDF();
    }
    
    function getPdfIntance(){
        return $this->_pdf;
    }
}