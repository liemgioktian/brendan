<?php

class form_handler{
    private $_temp;
    function __construct(){
        $this->_temp=array('img'=>array(),'pdf'=>array());
    }
    
    public function render_form($file,$data){        
        ob_start();
            if(isset($data['signature'])){
                foreach($data['signature'] as $key => $sig){
                    $data['signature'][$key] = $this->_draw_signature($sig);
                }
            }
            
            foreach ($data as $v => $k){
                $$v = $k;
            }
            
            include($file);
                        
            $html=ob_get_contents();
        ob_end_clean();
        
        $this->_generate_pdf($html);
    }
    
    private function _generate_pdf($source, $stream=true){
        include 'lib/pdf/dompdf_config.inc.php';
        $pdf = new DOMPDF;        
        
        $pdf->load_html($source);
        $pdf->render();
        
        if($stream) {
            $pdf->stream('form.pdf');
            $this->_clear_img_resource();
            return true;
        }else{
            $file_name  =str_replace(array('0.',' '),array('','_'),microtime());
            $pdf_path   ='temp/'.$file_name.'.pdf';
            
            $pdf_file=fopen($pdf_path,'w+');
            fwrite($pdf_file,$pdf->output());
            fclose($pdf_file);
            $this->_clear_img_resource();
            $this->_temp['pdf'][]=$pdf_path;
            return $pdf_path;
        }
    }
    
    private function _draw_signature($sig){
        if(!$sig) return 'assets/imgs/signature_blank.png';
        include_once ('signature.php');
        $signature = new Signature;
        
        $file_name  =str_replace(array('0.',' '),array('','_'),microtime());
        $img_path   ='temp/'.$file_name.'.png';
        
        imagepng($signature->createImage($sig),$img_path);
        $this->_temp['img'][]=$img_path;
        return $img_path;
    }
    
    private function _clear_img_resource(){
        foreach($this->_temp['img'] as $temp){
            unlink($temp);
        }
    }
    
    private function _clear_pdf_resource(){
        foreach($this->_temp['pdf'] as $temp){
            unlink($temp);
        }
    }
}