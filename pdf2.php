<?php
include_once 'lib/library/class.phpmailer.php';
include_once 'lib/library/dompdf/dompdf_config.inc.php';

function _generate_pdf($source, $filename='', $stream=true){
    $_pdf   =new DOMPDF;    
    
    $_pdf->load_html($source);
    $_pdf->render();
    
    if($stream) {
        $_pdf->stream($filename);
    }else{
        $file_name  =($filename)?$filename:str_replace(array('0.',' '),array('','_'),microtime());
        $file_name  =$file_name.'_'.str_replace(array('0.',' '),array(''),microtime());
        $pdf_path   ='temp/'.$file_name.'.pdf';
        
        $pdf_file=fopen($pdf_path,'w+');
        fwrite($pdf_file,$_pdf->output());
        fclose($pdf_file);
        if(file_exists($pdf_path)){
            return $pdf_path;
        }else{
            return false;
        }
    }
}

function _send_mail($address, $subject,$attachment){
    $_email =new PHPMailer;
    
    $_email->SetFrom('admin@admin.com','Brendan Hanks');
    $_email->Subject=$subject;
    
    $_email->MsgHTML('Report attached');
    $_email->IsMail();
    $_email->AddAttachment($attachment);
    $_email->AddAddress($address);
    $sent= $_email->Send();
    return $sent;
}



if($_POST){
    ob_start();
        if($_POST){

        include 'will_sheet_pdf.php';
        $subject    ="will_sheet({$_POST['surename']} - {$_POST['dob']})";
        $file_name  ="will_sheet_{$_POST['surename']}_{$_POST['dob']}";
        
        }

    $pdf_data=ob_get_contents();
    ob_end_clean();

    if($_POST['action'] == 'askquestion'){
        $_email = new PHPMailer;
        $_email->SetFrom('admin@admin.com','Brendan Hanks');
        $_email->Subject='Ask a Question';
        $_email->MsgHTML($_POST['a_question']);
        $_email->IsMail();
        $_email->AddAddress('admin@admin.com');
        echo $_email->Send()?1:0;
    }else if($_POST['action'] == 'pdf'){
        $_pdf   = new DOMPDF;
        $_pdf->load_html($pdf_data);
        $_pdf->render();
        
        $pdf_path   = 'temp/'.$file_name.'.pdf';
        $pdf_file = fopen($pdf_path,'w+');
        fwrite($pdf_file,$_pdf->output());
        fclose($pdf_file);
        if(file_exists($pdf_path)) _send_mail('admin@admin.com',$subject,$pdf_path);
            
        $_pdf->stream($file_name);
    }else{
    
        $attachment =_generate_pdf($pdf_data, $file_name, false);
        if($attachment){
            $success    =_send_mail($_POST['email'],$subject,$attachment);
            $status = ($success)?'sending email success':'failed to send email';
            if($success){
                sleep(3);
                unlink($attachment);
            }
        }else{
            $status = 'failed';
        }

        echo "<html><head><script>alert('$status');history.back()</script></head></html>";
    }
    
}
