$(document).ready(function(){

    var $calc = $('#brendan2').calx();

/*
*   button submit
*/
    $('#btn_pdf').click(function(e){
        e.preventDefault();
        $('#action').val('pdf');
        $('#brendan2').submit();
    });

    $('#email').click(function(e){
        e.preventDefault();
        $('#action').val('email');
        $('#brendan2').submit();
    })

/*
*   button ask.question
*/  
    $('#ask_part1').click(function(e){
        e.preventDefault();
        $('#ask_question').modal('show');
        $('[name="submit_question"]').click(function(){
            var button = $(this);
            button.html('please wait...');
            $.ajax({
               url :'pdf2.php',
               type :'POST',
               dataType :'text',
               data:{action:'askquestion',a_question:$('[name="ask1"]').val()},
               success:function(r){
                  $('#ask_question').modal('hide');
                  $('[name="ask1"]').val('');
                  $('[name="submit_question"]').html('Send');
                  $('[name="submit_question"]').off('click');
               }
            });
        });
    });
   
/** initial date-picker */
    $('.datepicker').datepicker({
        'dateFormat' : 'dd-mm-yy'
    });

/** find next input when enter */
    var $input = $('input');
    $input.on('keypress', function(e) {
        if (e.which === 13) {
            var ind = $input.index(this);
            $input.eq(ind + 1).focus();
            e.preventDefault();
        }
    });

/*
*   checkbox as radio button
*/

    $('input:checkbox').change(function() {
        
        if (this.checked) {
            var checkname = $(this).attr("name");
            $("input:checkbox[name='" + checkname + "']").not(this).removeAttr("checked").trigger("change");
        }
    });

/*
*   inserted box
*/

    $('.former3').click(function(){
        var y = $('#former32').val();
        if($('#former31').is(':checked')){
            $('#relation_prior').removeClass('hidden');
        }else if($('#former32').is(':checked')){
            $('#relation_prior').addClass('hidden');
        };
    });

/*
*   progress bar
*/
    $(function() {
    var clicks = 1;
    $('.next').on('click', function() {
    clicks++;
      var percent = Math.min(Math.round(clicks / 18 * 100), 100);
      $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 18');
    });

    $('.back').on('click', function(){
        clicks--;
        var percent = Math.min(Math.round(clicks / 18 *100), 100);
        $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 18');
    });

    });

/*
*   back next button
*/
    var $jq = window.parent.jQuery;
    var $iframeContainer = $jq('#assesmentContainer');

    $iframeContainer.css('height', $('body').height()+20);

    $('.back, .next').click(function(){
        var $button = $(this);
        var $prevPage = $button.attr('data-back');
        var $nextPage = $button.attr('data-next');

        $('.page').addClass('hidden');

        if($button.hasClass('next')){
            $('#page'+$nextPage).removeClass('hidden');
        }else{
            $('#page'+$prevPage).removeClass('hidden');
        }

        $iframeContainer.css('height', $('body').height()+20);

    });

/*
*   add more row
*/  
    $col_num = 2;
    $row_num1 = 4;
    $row_num2 = 4;
    $row_num3 = 4;
    
    $('#add_dependants').click(function(e){
        e.preventDefault();
        
        $row_num1++;

        var $dependants_row = $('#dependants_row');
        
        $dependants_row.append(
            
            '<tr>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="name_depend'+$row_num1+'" name="name_depend'+$row_num1+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="relation_depend'+$row_num1+'" name="relation_depend'+$row_num1+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="address_depend'+$row_num1+'" name="address_depend'+$row_num1+'">\
                        </div>\
                    </div>\
                </td>\
            </tr>'
            
            );
        var count = $('[name="count_dependants"]');
        count.val(parseInt(count.val())+1);
    });

    $('#add_invest_assets').click(function(e){
        e.preventDefault();
        
        $row_num2++;
        var $invest_assets_row = $('#invest_assets_row');
        
        $invest_assets_row.append(

            '<tr>\
                <td class="text-center">\
                    <input type="text" class="hidden" id="invest_no'+$row_num2+'" name="invest_no'+$row_num2+'" value="'+$row_num2+'">\
                    <label for="" id="investment_no'+$row_num2+'" name="investment_no'+$row_num2+'" data-formula="$invest_no'+$row_num2+'"></label>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="invest_account'+$row_num2+'" name="invest_account'+$row_num2+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="invest_owner'+$row_num2+'" name="invest_owner'+$row_num2+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="invest_value'+$row_num2+'" name="invest_value'+$row_num2+'" data-format="$ 0,0[.]">\
                        </div>\
                    </div>\
                </td>\
            </tr>'

        );
        $calc.calx('refresh');
        var count = $('[name="count_invest_assets"]');
        count.val(parseInt(count.val())+1);
    });

    $('#add_invest_property').click(function(e){
        e.preventDefault();
        
        $row_num3++;
        var $invest_property_row = $('#invest_property_row');
        
        $invest_property_row.append(
            '<tr>\
                <td class="text-center">\
                    <input type="text" class="hidden" id="property_no'+$row_num3+'" name="property_no'+$row_num3+'" value="'+$row_num3+'">\
                    <label for="" id="invest_property_no'+$row_num3+'" name"invest_property_no'+$row_num3+'" data-formula="$property_no'+$row_num3+'"></label>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="property_address'+$row_num3+'" name="property_address'+$row_num3+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="property_owner'+$row_num3+'" name="property_owner'+$row_num3+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm datepi" id="property_date'+$row_num3+'" name="property_date'+$row_num3+'">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                         <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="property_value'+$row_num3+'" name="property_value'+$row_num3+'" data-format="$ 0,0[.]">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="property_balance'+$row_num3+'" name="property_balance'+$row_num3+'" data-format="$ 0,0[.]">\
                        </div>\
                    </div>\
                </td>\
            </tr>'
        );
        $calc.calx('refresh');
        var count = $('[name="count_invest_property"]');
        count.val(parseInt(count.val())+1);
    });

    $('#add_liabilities').click(function(e){
        e.preventDefault();
        $col_num++;
        var $liabilities_row = $('#liabilities_row');
        $liabilities_row.append(
            '<tr>\
                <td class="">\
                    <label for="">Investment Loan '+$col_num+'</label>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="liabilitas_loan'+$col_num+'" name="liabilitas_loan'+$col_num+'" data-format="$ 0,0[.]">\
                        </div>\
                    </div>\
                </td>\
                <td>\
                    <div class="col-sm-12">\
                        <div class="form-group">\
                            <input type="text" class="form-control input-sm" id="liabilitas_bank'+$col_num+'" name="liabilitas_bank'+$col_num+'">\
                        </div>\
                    </div>\
                </td>\
            </tr>'
        );
        var count = $('[name="count_liabilities"]');
        count.val(parseInt(count.val())+1);
    })

/*
*   
*/
    
});